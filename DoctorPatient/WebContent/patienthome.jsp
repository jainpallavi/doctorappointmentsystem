<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="java.sql.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
            $(function () {
                $("#datepicker").datepicker({ dateFormat: 'yy/mm/dd' });
               
            });
            
            
            
            $(document).ready(function() {

            $('#speciality').change(function(event) {
                    var speciality = $("select#speciality").val();
                    $.get( {
                            specialityName : speciality
                    }, function(response) {

                    var select = $('#docname');
                    select.find('option').remove();
                      $.each(response, function(index, value) {
                      $('<option>').val(value).text(value).appendTo(select);
                  });
                    });
                    });
            });
            </script>
          
            
            
       
     <title>Book An Appointment</title>
<style type="text/css">
body{
background-image: url("images/doctor1.png");
  background-color: #cccccc;
  background-repeat: no-repeat;
    background-size: cover;
  height: 500px;
  background-blend-mode: lighten;
 

}
	
.container {
	border-radius: 50px;

	box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.29);
	padding: 2px;
}
	.roundbox 	{
    	display: inline-block;
    	height: 40px;
    	background-color: #9C9E9E;
    	width: 180px;
    	text-align: center;
    	line-height: 40px;
    	color:yellow;
    	border-radius:30px;
    	cursor: pointer;
    	text-decoration: none;
		color:yellow;
	}
			
	 .reg td	{
   		text-align: center;
   		font-size:20px ;
  		   }
  	
.text-block {
		display: inline-block;
    	height: 80px;
    	background-color: #9C9E9E;
    	width: 180px;
    	text-align: center;
    	line-height: 40px;
    	border-radius:30px;
    	cursor: pointer;
    	text-decoration: none;
		color:yellow;
}	
  	   
	.welcome	{
  		display: inline-block;
    	float: right;
       	width: 370px;
  		line-height: 40px;
       	border-radius:30px;
      	cursor: pointer;
       }
    .reg table{
        width: 500px; height: 425px; border-style:50px;border-radius:20px;
        }    
    .reg input[type="text"],input[readonly="readonly"],input[type="number"],input[type="text"],input[type="password"],input[type="date"],select {	
      width: 300px; height: 35px; 
       }
      .reg input[readonly="readonly"]	{
      	background-color: #E2EAEA;
      } 
    .reg textarea   {
    	width: 300px;
    	height: 50px;
    	resize:none;
    }
    .reg input[type="submit"]{
      width: 300px; height: 35px;  border-bottom: 2px solid black;
      border-radius:30px;
      }
      
      option	{
      	font-size:20px;
      	font-family: cursive;
      }
</style>
</head>
<body bgcolor="#ADD8E6">
<jsp:include page="header.jsp" />
	
	<br>
	<div class="container">
	<ul>
			<a href="patienthome.jsp"> <li class="roundbox">Book An Appointment</li></a>
			<a href="PatientUpdate.jsp"> <li class="roundbox">Update Profile</li></a>
			<a href="ViewAppointment.jsp"><li class="roundbox">View Appointment</li></a>
			
			<a href="logout.jsp"><li class="roundbox">Logout</li></a>
 			<li class="welcome"><marquee> 

	
		<%  
         response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); //HTTP 1.1
         response.setHeader("Pragma", "no-cache");	  //HTTP 1.0    
         response.setHeader("Expires", "0");  	//	Proxies 	
				
			       if(session.getAttribute("email")==null)
			       {
 					response.sendRedirect("PatientLogin.jsp");
 					}
 			       else if(session!=null)	{
 					String email=(String) session.getAttribute("email");
 					out.println("Hello ,  "+email);
 					
 			       
 			       
				%>
	}
	</marquee>
		</ul>
		<center>	
		<h1>Book An Appointment</h1>
		<div class="reg">
		<form action="AppointmentReg" method="get">
		<table>
		<tr><td></td></tr>
		<tr><td>Name:</td><td><input type="text" name="name" placeholder="Name" required></td></tr>
		
		<tr><td>Email:</td><td><input type="text" name="email" value="<%out.print(email);}
		%>"></td></tr>
	
		<tr><td>Contact No:</td><td><input type="text" name="contact" pattern="^\d{10}$"  placeholder="xxxxxxxxx"  required   ></td></tr>
		
		<tr><td>Age:</td><td><input type="number" name="age" min="1" max="100" required></td> </tr>
		<tr><td>Specialty:</td><td><select name="specialityName" id="specialityName" class="form_control" >
		<option>Select Specialisation</option>
		<%
		String username = "root";
		String pass = "rat";
		String url = "jdbc:mysql://localhost:3306/doctorappointment"; 
      
        	Class.forName("com.mysql.jdbc.Driver");
        	Connection con=DriverManager.getConnection(url,username,pass);  
            PreparedStatement ps=con.prepareStatement("select distinct speciality from doctor");  
            ResultSet rs=ps.executeQuery(); 
            while(rs.next()){
		%>
		<option value="<%=rs.getString("speciality")%>"><%=rs.getString("speciality") %></option>
		<%
            }
		%>
		</select>
    	</td></tr>
		
		<tr><td>Doctor:</td><td><select name="doctor"  class="form_control" id="docname" required>
		
      <option>Select doctor</option>
      
      <%
		String uname = "root";
		String pas = "rat";
		String ur= "jdbc:mysql://localhost:3306/doctorappointment"; 
		String specialityName=(String)session.getAttribute("speciality");
        	Class.forName("com.mysql.jdbc.Driver");
        	Connection conn=DriverManager.getConnection(ur,uname,pas);  
            PreparedStatement ps1=conn.prepareStatement("select docname from doctor where speciality="+specialityName+"");  
            ResultSet rs1=ps1.executeQuery(); 
            while(rs1.next()){
		%>
<option value="<%=rs1.getString("docname")%>"></option>		<%
            }
		%>
      </select>
      </td>
      </tr>
            
       
	           
        
		<tr><td>Appointment Date</td><td><input type="text" name="appointmentdate" id=datepicker required></td>
		</tr>
		<tr><td>Choose Time Slot:</td><td><select name="slot">
		<option>Select time</option>
		<option value="6:00-6:15PM">6:00-6:15PM</option>
    	<option value="6:15-6:30PM">6:15-6:30PM</option>
    	<option value="6:30-6:45PM">6:30-6:45PM</option>
       	<option value="6:45-7:00PM">6:45-7:00PM</option>
    	<option value="7:00-7:15PM">7:00-7:15PM</option>
    	<option value="7:15-7:30PM">7:15-7:30PM</option>
    	<option value="7:30-7:45PM">7:30-7:45PM</option>
    	<option value="7:45-8:00PM">7:45-8:00PM</option>
    
    </select></td></tr>
		
		
		<tr><td>Description</td><td><textarea name="description" placeholder="Enter Your Health Status.." required="required"></textarea></td></tr>
		
		<tr><td><td><input type="submit" value="Submit"></td></tr>
	</table>
	</form>
	</div>
	</center>
	<div class="text-block">
    <a href="DoctorList.jsp" target="_blank"> <h2>Doctor List</h2> </a>
   </div>
	<hr	>
	<br></div>
</body>
</html>

